from django.contrib.auth.models import User as djangoUser
from django.db.models.fields import EmailField
from rest_framework.authtoken.models import Token
from rest_framework.response import Response
from rest_framework import status
from .models import Teacher

def process_sso_profile(sso_profile):
    try:
        user = djangoUser.objects.get(username=sso_profile['email'])
        token, _ = Token.objects.get_or_create(user=user)
    except djangoUser.DoesNotExist:
        user = djangoUser(username=sso_profile['email'])
        user.set_unusable_password()
        user.save()
        generate_user_profile(user)
        token = Token.objects.create(user=user)
    return token.key

def generate_user_profile(user):
	return Teacher.objects.create(
        user=user,
        namaLengkap='paw',
        email='test',
        isTeacher = True
    )

def response(status = status.HTTP_200_OK, data = None, error = None):
    return Response({
					"data": data,
					"error": error,
				}, status=status)

def validateParams(request, params):
    for param in params:
        res = request.query_params.get(param)
        if res is None:
            return response(error="{} is required".format(param), status=status.HTTP_404_NOT_FOUND)
    return None
	
def validateBody(request, attrs):
    for attr in attrs:
        res = request.data.get(attr)
        if res is None:
            return response(error="{} is required".format(attr), status=status.HTTP_404_NOT_FOUND)
    return None