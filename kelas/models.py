from django.db import models
from django.db.models.deletion import CASCADE
from django.contrib.auth.models import User
import uuid


class User(models.Model):
    user = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)
    namaLengkap = models.CharField(max_length=10)
    tanggalLahir = models.CharField(max_length=10)
    email = models.CharField(max_length=10)
    pendidikanTerakhir = models.CharField(max_length=10)
    alamat = models.CharField(max_length=10)
    jenisKelamin = models.CharField(max_length=10)
    isTeacher = models.BooleanField(default=False)
    isStudent = models.BooleanField(default=False)
    isAdmin = models.BooleanField(default=False)

    class Meta:
        abstract = True

    def __str__(self):
        return self.name

class Teacher(User):
    isTeacher = models.BooleanField(default=True)
    lisensi = models.CharField(max_length=10)
    isVerified = models.BooleanField(default=False)

    def __str__(self):
        return self.name

class Student(User) :
    isStudent = models.BooleanField(default=True)

    def __str__(self):
        return self.name

class Admin(User) :
    isAdmin = models.BooleanField(default=True)

    def __str__(self):
        return self.name

class Content(models.Model):
    judul = models.CharField(max_length=30, default="False")
    deskripsi = models.CharField(max_length=30, default="False")
    videoUrl = models.CharField(max_length=30, default="False")

    def __str__(self):
        return self.name

class Kelas(models.Model):
    nama = models.CharField(max_length=30)
    deskripsi = models.CharField(max_length=2048, blank=True)
    kode_kelas = models.CharField(max_length=10, unique=True, default=uuid.uuid4)
    is_publish = models.BooleanField(default=False)
    is_verified = models.BooleanField(default=False)
    daftarStudent = models.ManyToManyField(Student)
    daftarContent = models.ManyToManyField(Content)
    daftarTeacher = models.ManyToManyField(Teacher)

    def __str__(self):
        return self.name

