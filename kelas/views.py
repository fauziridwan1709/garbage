from django.shortcuts import render

import json
from django_filters.rest_framework.backends import DjangoFilterBackend
from rest_framework.decorators import api_view, permission_classes
from rest_framework import permissions, status
from rest_framework.fields import CreateOnlyDefault
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.filters import SearchFilter
from rest_framework import viewsets
from datetime import datetime
from django.core import serializers
from django.db.models import Count, Prefetch
from django.db import transaction
from .utils import process_sso_profile, response, validateBody, validateParams

from .models import  Kelas, Teacher, User
from .serializers import KelasSerializer
from django.http.response import HttpResponseRedirect
from django.shortcuts import redirect
import logging


logger = logging.getLogger(__name__)

# TODO: Refactor login, logout, token to viewset
@api_view(['POST'])
@permission_classes((permissions.AllowAny,))
def login(request):
	"""
	Handle UI login.
	"""
	
	body_unicode = request.body.decode('utf-8')
	body = json.loads(body_unicode)
	email = body['email']
	if email is not None:
	    token = process_sso_profile(body)
	    username = body['email']
	    return HttpResponseRedirect(
	            '/token?token=%s&username=%s' % (token, username))
			
	data = {'message': 'invalid sso'}
	return Response(data=data, status=status.HTTP_401_UNAUTHORIZED)

@api_view(['GET'])
@permission_classes((permissions.AllowAny,))
def token(request):
	return Response(request.GET)

@api_view(['GET'])
def logout(request):
	"""
	Handle UI logout.
	"""
	return HttpResponseRedirect(get_logout_url(request))


@api_view(['GET', 'PUT', 'POST','DELETE'])
def kelas(request):
	"""
	Handle CRUD Kelas.
	Remember that this endpoint require Token Authorization. 
	"""


	print("test test test")
	print(request.user)
	user = Teacher.objects.get(username=str(request.user))
	# Melihat daftar kelas (Ravi)
	if request.method == 'GET':

		#isById = validateParams(request, ['id'])
		# if isById == None:
		# 	return get_review_by_id(request)
		
		# is_valid = validateParams(request, ['course_code'])
		# if is_valid != None:
		# 	return is_valid
		# code = request.query_params.get("course_code")
		
		# course = Course.objects.filter(code=code).first()
		# if course is None:
		# 	return response(error="Course not found", status=status.HTTP_404_NOT_FOUND)
		kelas = Kelas.objects.filter(is_publish=False)
		if kelas.exists():
			#review_likes = ReviewLike.objects.filter(review__course=course)
			#review_tags = ReviewTag.objects.all()
			return response(data=KelasSerializer(kelas, many=True).data)
		return response(data=[])
		
	# Membuat kelas (Fauzi)
	if request.method == 'POST':
		is_valid = validateBody(request, ['nama', 'deskripsi'])
		if is_valid != None:
			return is_valid

		nama = request.data.get("nama")
		deskripsi = request.data.get("deskripsi")

		try:
			with transaction.atomic():
				review = Kelas.objects.create(
					nama=nama,
					deskripsi=deskripsi,
				)
				review.daftarTeacher.add(user)
		except Exception as e:
			logger.error("Failed to save review, request data {}".format(request.data))
			return response(error="Failed to save review, error message: {}".format(e), status=status.HTTP_404_NOT_FOUND)

		return response(data=KelasSerializer(review).data, status=status.HTTP_201_CREATED)
		
	# Memperbarui kelas (Fauzi)
	if request.method == 'PUT':
		is_valid = validateBody(request, ['kelas_id'])
		if is_valid != None:
			return is_valid

		kelas_id = request.data.get("kelas_id")
		content = request.data.get("content")

		review = Kelas.objects.filter(user=user, id=kelas_id).first()
		if review is None:
			return response(error="Review does not exist", status=status.HTTP_409_CONFLICT)

		review.content = content
		review.save()

		return response(data=KelasSerializer(review).data, status=status.HTTP_201_CREATED)
	

	# Menghapus kelas (Ravi)
	if request.method == 'DELETE':
		is_valid = validateParams(request, ['kelas_id'])
		if is_valid != None:
			return is_valid

		kelas_id = request.query_params.get("kelas_id")
		
		kelas = Kelas.objects.filter(id=kelas_id).first()
		if kelas is None:
			return response(error="Kelas does not exist", status=status.HTTP_409_CONFLICT)
		#kelas.is_active = False
		kelas.delete()
		return response(status=status.HTTP_200_OK)
