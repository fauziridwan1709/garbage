from rest_framework import serializers

from .models import Content, Kelas, Teacher, Student, Admin, User

class KelasSerializer(serializers.ModelSerializer):
    # likes_by = serializers.SerializerMethodField('get_likes')
    # tags = serializers.SerializerMethodField('get_tags')
    # author = serializers.SerializerMethodField('get_author')
    # kode_kelas = serializers.SerializerMethodField('get_kode_kelas')
    class Meta:
        model = Kelas
        fields = ('id', 'nama','deskripsi','kode_kelas','is_publish','is_verified')

class ContentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Content
        fields = ('judul', 'deskripsi', 'videoUrl')

    # def get_author(self, obj):
    #     return obj.user.username

    # def get_teachers(self, obj):
    #     return obj.course.code
    
    # def get_likes(self, obj):
    #     try:
    #         review_likes = self.context['review_likes'].filter(review=obj)
    #     except:
    #         review_likes = []
    #     likes = []
    #     for like in review_likes:
    #         likes.append(like.user.username)
    #     return likes

    # def get_tags(self, obj):
    #     try:
    #         review_tags = self.context['review_tags'].filter(review=obj)
    #     except:
    #         review_tags = []
    #     tags = []
    #     for tag in review_tags:
    #         tags.append(tag.tag.tag_name)
    #     return tags

