from django import conf
from rest_framework import routers
from django.urls import path, include
from .views import  kelas

router = routers.SimpleRouter()

urlpatterns = [
    # path("", include(router.urls)),
    path("", kelas, name="kelas"),
] + router.urls