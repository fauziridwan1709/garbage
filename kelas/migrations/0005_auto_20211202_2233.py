# Generated by Django 3.1.2 on 2021-12-02 15:33

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('kelas', '0004_remove_teacher_daftarkelas'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='content',
            name='content',
        ),
        migrations.AddField(
            model_name='content',
            name='deskripsi',
            field=models.CharField(default='False', max_length=30),
        ),
        migrations.AddField(
            model_name='content',
            name='judul',
            field=models.CharField(default='False', max_length=30),
        ),
        migrations.AddField(
            model_name='content',
            name='videoUrl',
            field=models.CharField(default='False', max_length=30),
        ),
    ]
