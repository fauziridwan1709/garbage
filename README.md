# esehat Backend

Description.

### Create virtual environment

```bash
python3 -m venv env

# Activate virtual environment
source env/bin/activate

# How to Deactivate
deactivate
```

### Run app

1. activate env and install requirement
```bash
source env/bin/activate
pip install -r requirements.txt
```

2. run project
```bash
python manage.py runserver
```
